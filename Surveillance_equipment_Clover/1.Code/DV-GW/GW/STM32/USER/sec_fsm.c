#include "sec_fsm.h"
#include "sec_sys.h"


sec_FrameMsg_t data_in;
secValueAll_t sec_vr_all_fsm_t;
fsm_state_e fsm_state = FSM_STATE_START_FRAME;

static uint16_t timeout_fsm = 0;
static uint8_t  flag_timeout_fsm = 0;
static uint16_t checkframe_receive;


void SEC_TimeOutFsm(void)
{
    if(flag_timeout_fsm == 1)
    {
        if(timeout_fsm == FSM_TIMEOUT)
        {
			printf("101\n");
            SEC_ClearTimeOutFsm();
        }
        timeout_fsm++;
    }
}

void SEC_ClearTimeOutFsm(void)
{
    flag_timeout_fsm = 0;
    sec_vr_all_fsm_t.fsm_count_array = 0;
    fsm_state = FSM_STATE_START_FRAME;
}

void SEC_ReceiveMessageFsm(uint8_t temp_data)
{
	flag_timeout_fsm  = 1;
    timeout_fsm = 0;
	
	switch (fsm_state)
    {
        case FSM_STATE_START_FRAME:
            sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array] = temp_data;
            sec_vr_all_fsm_t.fsm_count_array++;

            if (sec_vr_all_fsm_t.fsm_count_array == FSM_STATE_CHANGE_VALUE_TYPE_MESSAGE)
            {
                data_in.StartFrame = SEC_ConvertFromBytesToUint16(sec_vr_all_fsm_t.array_out[0], sec_vr_all_fsm_t.array_out[1]);
                if(data_in.StartFrame == START_BYTE)
                {
                    fsm_state = FSM_STATE_TYPE_MESSAGE;
                }
                else
                {
                    sec_vr_all_fsm_t.fsm_count_array = 0;
                    fsm_state = FSM_STATE_START_FRAME;
                }
            }
        break;

        case FSM_STATE_TYPE_MESSAGE:
            sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array] = temp_data;
            sec_vr_all_fsm_t.fsm_count_array++;

            if (sec_vr_all_fsm_t.fsm_count_array == FSM_STATE_CHANGE_VALUE_PORT_NUMBER)
            {
                if(sec_vr_all_fsm_t.array_out[2] <= 4)
                {
                    fsm_state = FSM_STATE_PORT_NUMBER;
                }
                else
                {
                    sec_vr_all_fsm_t.fsm_count_array = 0;
                    fsm_state = FSM_STATE_START_FRAME;
                }
            }
        break;

        case FSM_STATE_PORT_NUMBER:
            sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array] = temp_data;
            sec_vr_all_fsm_t.fsm_count_array++;
            if (sec_vr_all_fsm_t.fsm_count_array == FSM_STATE_CHANGE_VALUE_LENGHT_DATA)
            {
                if(sec_vr_all_fsm_t.array_out[3] <= 6)
                {
                    fsm_state = FSM_STATE_LENGHT_DATA;
                }
                else
                {
                    sec_vr_all_fsm_t.fsm_count_array = 0;
                    fsm_state = FSM_STATE_START_FRAME;
                }
            }
        break;

        case FSM_STATE_LENGHT_DATA:
            sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array] = temp_data;
            sec_vr_all_fsm_t.fsm_count_array++;

            if (sec_vr_all_fsm_t.fsm_count_array == FSM_STATE_CHANGE_VALUE_END)
            {
                sec_vr_all_fsm_t.fsm_data_length = ((sec_vr_all_fsm_t.array_out[5] << 8) | sec_vr_all_fsm_t.array_out[4]) + 6;

                if (sec_vr_all_fsm_t.fsm_data_length < 24)
                { 
                    fsm_state = FSM_STATE_END;
                }
                else if (sec_vr_all_fsm_t.fsm_data_length > 24)
                {
                    sec_vr_all_fsm_t.fsm_count_array = 0;
                    fsm_state = FSM_STATE_START_FRAME;
                }
            }
        break;

        case FSM_STATE_END:
            sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array] = temp_data;
            sec_vr_all_fsm_t.fsm_count_array++;

            if (sec_vr_all_fsm_t.fsm_count_array == sec_vr_all_fsm_t.fsm_data_length)
            {
				data_in.CheckFrame = SEC_CheckSum(sec_vr_all_fsm_t.array_out, sec_vr_all_fsm_t.fsm_data_length - 2);
                checkframe_receive = SEC_ConvertFromBytesToUint16(sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array-2], sec_vr_all_fsm_t.array_out[sec_vr_all_fsm_t.fsm_count_array-1]);
				
                if(data_in.CheckFrame == checkframe_receive)
                {
                    SEC_ClearTimeOutFsm();
                    sec_vr_all_fsm_t.flag_fsm_true = 1;
                }
                else
                {
                    SEC_ClearTimeOutFsm();
                    sec_vr_all_fsm_t.flag_fsm_true = 0;
                }
            }
        break;
    }
	
	sec_vr_all_fsm_t.flag_fsm = 1;
}