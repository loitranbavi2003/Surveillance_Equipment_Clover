#ifndef __SEC_GPIO_H__
#define __SEC_GPIO_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "sec_sys.h"

typedef enum
{
	GET_GPIO_CONNECTED = 0,
	GET_GPIO_DISCONNECTED
}_status_port_plug_e;

typedef enum 
{
	GPIO_CONTROL_ENA_ALL = 0,
	GPIO_CONTROL_DIS_ALL,
} gpio_Control_e;

void SEC_GpioInit(void);
void SEC_SetGpioPortInit(void);
void SEC_GetGpioPortInit(void);
void SEC_GpioSetOn(int port);
void SEC_GpioSetOff(int port);
void SEC_TestGpioControl(gpio_Control_e stt);

#ifdef __cplusplus
}
#endif

#endif

/********************************* END OF FILE ********************************/
/******************************************************************************/
