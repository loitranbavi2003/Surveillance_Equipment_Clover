#include "sec_split_time.h"


sec_FrameMsg_t frame_detect;
sec_status_time_e status_time = STATUS_START;

Array_port_t arr_port[] =
{
  {GPIOB, GET_GPIO_PORTB_1},
  {GPIOB, GET_GPIO_PORTB_2},
  {GPIOB, GET_GPIO_PORTB_3},
  {GPIOB, GET_GPIO_PORTB_4},
  {GPIOB, GET_GPIO_PORTB_5},
  {GPIOB, GET_GPIO_PORTB_6}
};

static uint8_t count_port = 1;
static uint8_t count_timeout = 0;
static uint8_t count_time_send = 0;

static uint8_t arr_status_new[NUMBER_PORT]      = {0};
static uint8_t arr_status_old[NUMBER_PORT]      = {0};
static uint8_t arr_status_standard[NUMBER_PORT] = {0};
static uint8_t arr_count_error[NUMBER_PORT]   = {0};
static uint8_t flag_change_status[NUMBER_PORT]  = {0};


void SEC_SplitFrameTime(void)
{
	switch (status_time)
	{
		case STATUS_START:
			SEC_ReadStatusPort(count_port);
			status_time = STATUS_JOIN;
		break;

		case STATUS_JOIN:
			if (flag_change_status[count_port] == 1)
			{
				printf("Port: %d, new: %d, old: %d, sta: %d\n", count_port, arr_status_new[count_port], arr_status_old[count_port], arr_status_standard[count_port]);
				if (arr_status_standard[count_port] == SENSOR_JOIN)
				{
					if (count_time_send == 0)
					{
						SEC_JoinPortSend(count_port);
					}
					count_time_send++;

					if (count_time_send == TIME_SEND)
					{
						count_time_send = 0;
						if (SEC_JoinPortReceive(count_port) == 1)
						{
							arr_status_standard[count_port] = SENSOR_CONNECT;
							flag_change_status[count_port] = 0;
							status_time = STATUS_DISCONNECT;
						}
					}
				}
				else if (arr_status_standard[count_port] == SENSOR_DISCONNECT)
				{
					SEC_GpioSetOff(count_port);
					flag_change_status[count_port] = 0;
					status_time = STATUS_DISCONNECT;
				}
			}
			else
			{
				status_time = STATUS_DISCONNECT;
			}
		break;

		case STATUS_DISCONNECT:
			status_time = STATUS_DATA;
		break;

		case STATUS_DATA:
			if (arr_status_standard[count_port] == SENSOR_CONNECT)
			{
				if (count_time_send == 0)
				{
					SEC_DataPortSend(count_port);
				}
				count_time_send++;

				if (count_time_send == TIME_SEND)
				{
					count_time_send = 0;
					if (SEC_DataPortReceive(count_port) == 1)
					{
						arr_count_error[count_port] = 0;
						status_time = STATUS_BACK;
					}
				}
			}
			else
			{
			status_time = STATUS_BACK;
			}
		break;

		case STATUS_BACK:
			count_port++;
			if (count_port >= 7)
			{
				count_port = 1;
			}
			status_time = STATUS_START;
		break;
	}
}

void SEC_JoinPortSend(uint8_t port_number)
{
	// Bat nguon port
	SEC_GpioSetOn(port_number);
	// Gui ban tin hoi type sensor
	SEC_CreateFrameAsksensor(port_number);

	#if FRAME_SEND
		printf("Send type_%d\n", port_number);
	#endif
}

uint8_t SEC_JoinPortReceive(uint8_t port_number)
{
	if (sec_vr_all_fsm_t.flag_fsm_true == 1)
	{
		#if FRAME_RECEIVE
			printf("Ask_%d\n", port_number);
		#endif
		// Giai ma ban tin nhan duoc
		SEC_MessageDetectFrame(sec_vr_all_fsm_t.array_out, &frame_detect);
		// Hien thi ban tin nhan duoc
		SEC_PrintDataDetect(frame_detect);
		// Reset cac gia tri trong Struct
		SEC_ClearFrameMessage(&frame_detect);
		sec_vr_all_fsm_t.flag_fsm_true = 0;
		return 1;
	}
	return 0;
}

void SEC_DataPortSend(uint8_t port_number)
{
	// Gui ban tin hoi data sensor
	SEC_CreateFrameAskdata(port_number);

	#if FRAME_SEND
		printf("Send data_%d\n", port_number);
	#endif
}

uint8_t SEC_DataPortReceive(uint8_t port_number)
{
	if (sec_vr_all_fsm_t.flag_fsm_true == 1)
	{
		#if FRAME_RECEIVE
			printf("Data_%d\n", port_number);
		#endif
		// Giai ma ban tin nhan duoc
		SEC_MessageDetectFrame(sec_vr_all_fsm_t.array_out, &frame_detect);
		// Hien thi ban tin nhan duoc
		SEC_PrintDataDetect(frame_detect);
		// Reset cac gia tri trong Struct
		SEC_ClearFrameMessage(&frame_detect);
		sec_vr_all_fsm_t.flag_fsm_true = 0;
		return 1;
	}
	return 0;
}

void SEC_ReadStatusPort(uint8_t port_number)
{
	if(GPIO_ReadInputDataBit(arr_port[port_number - 1].GPIOx, arr_port[port_number - 1].Port_number) == 0)
	{
		arr_status_new[port_number] = 1;
	}
	else
	{
		arr_status_new[port_number] = 0;
	}

	if(arr_status_new[port_number] != arr_status_old[port_number])
	{
		// Bat co thay doi trang thai
		flag_change_status[port_number] = 1;

		if ((arr_status_old[port_number] == 0) && (arr_status_new[port_number] == 1))
		{
			arr_status_standard[port_number] = SENSOR_JOIN;
		}
		else if ((arr_status_old[port_number] == 1) && (arr_status_new[port_number] == 0))
		{
			arr_status_standard[port_number] = SENSOR_DISCONNECT;
		}
		// Cap nhat lai trang thai
		arr_status_old[port_number] = arr_status_new[port_number];
	}
}

