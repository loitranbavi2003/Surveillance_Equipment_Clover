#include "sec_sys.h"

uint16_t count_time = 0;

void SEC_SysInit(void)
{	
	SEC_GpioInit();
	SEC_DelayInit(72);
	SEC_UartInit1(9600);
	SEC_UartInit2(9600);
}


void SEC_SysRun(void)
{
	SEC_TimeOutFsm();
	count_time++;
	if(count_time == 20)
	{
		SEC_SplitFrameTime();
		count_time = 0;
	}
	SEC_Delay_ms(1);
}
