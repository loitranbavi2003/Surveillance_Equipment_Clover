/*
  ******************************************************************************
  * @file		delay.h                                                            *
  * @author	Luu Ngoc Anh                                                       *
  * @date		18/07/2022                                                         *
  ******************************************************************************
*/
	
#ifndef __SEC_DELAY__H__
#define __SEC_DELAY__H__

#ifdef __cplusplus
 extern "C" {
#endif

void SEC_DelayInit (unsigned char clk);
void SEC_Delay_us (unsigned long time);
void SEC_Delay_ms (unsigned int time);

#ifdef __cplusplus
}
#endif

#endif

/********************************* END OF FILE ********************************/
/******************************************************************************/
