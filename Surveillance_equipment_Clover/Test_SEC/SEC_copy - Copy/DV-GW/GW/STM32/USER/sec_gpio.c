#include "sec_gpio.h"



void SEC_GpioInit(void)
{
	SEC_SetGpioPortInit();
	SEC_GetGpioPortInit();
	SEC_SetGpioPortRs485Init();
}

void SEC_TestGpioControl(gpio_Control_e stt)
{
	switch(stt)
	{
		case GPIO_CONTROL_ENA_ALL:
				SEC_GpioSetOn(1);
				SEC_GpioSetOn(2);
				SEC_GpioSetOn(3);
				SEC_GpioSetOn(4);
				SEC_GpioSetOn(5);
				SEC_GpioSetOn(6);
			break;
		
		case GPIO_CONTROL_DIS_ALL:
				SEC_GpioSetOff(1);
				SEC_GpioSetOff(2);
				SEC_GpioSetOff(3);
				SEC_GpioSetOff(4);
				SEC_GpioSetOff(5);
				SEC_GpioSetOff(6);
			break;
	}
}

void SEC_GpioSetOn(int port)
{
	switch(port)
	{
		case 1:
		{
			GPIO_SetBits(GPIOB, SET_GPIO_PORTB_1);
			break;
		}
		case 2:
		{
			GPIO_SetBits(GPIOB, SET_GPIO_PORTB_2);
			break;			
		}
		case 3:
		{
			GPIO_SetBits(GPIOB, SET_GPIO_PORTB_3);
			break;			
		}
		case 4:
		{
			GPIO_SetBits(GPIOB, SET_GPIO_PORTB_4);
			break;			
		}
		case 5:
		{
			GPIO_SetBits(GPIOA, SET_GPIO_PORTA_5);
			break;			
		}
		case 6:
		{
			GPIO_SetBits(GPIOA, SET_GPIO_PORTA_6);
			break;				
		}
		default:
			break;
	}	
}

void SEC_GpioSetOff(int port)
{
	switch(port)
	{
		case 1:
		{
			GPIO_ResetBits(GPIOB, SET_GPIO_PORTB_1);
			break;
		}
		case 2:
		{
			GPIO_ResetBits(GPIOB, SET_GPIO_PORTB_2);
			break;			
		}
		case 3:
		{
			GPIO_ResetBits(GPIOB, SET_GPIO_PORTB_3);
			break;			
		}
		case 4:
		{
			GPIO_ResetBits(GPIOB, SET_GPIO_PORTB_4);
			break;			
		}
		case 5:
		{
			GPIO_ResetBits(GPIOA, SET_GPIO_PORTA_5);
			break;			
		}
		case 6:
		{
			GPIO_ResetBits(GPIOA, SET_GPIO_PORTA_6);
			break;				
		}
		default:
			break;
	}
}

void SEC_SetGpioPortInit(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin  = SET_GPIO_PORTB_1|SET_GPIO_PORTB_2|SET_GPIO_PORTB_3|SET_GPIO_PORTB_4;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = SET_GPIO_PORTA_5|SET_GPIO_PORTA_6;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void SEC_GetGpioPortInit(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);	
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin   = GET_GPIO_PORTB_ALL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}
