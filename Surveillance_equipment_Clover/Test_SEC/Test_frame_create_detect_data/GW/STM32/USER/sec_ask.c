#include "sec_ask.h"


sec_FrameMsg_t frame_sensor_detect;


/*---------------------------------------------- (- Type Sensor -) ----------------------------------------------*/

uint8_t Create_Frame_AskSensor_Port_1(void)
{
	uint8_t arr_data_answertype[24], length_arr = 0;
	sec_FrameMsg_t frame_sensor_test;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
	frame_sensor_test.PortNumber  = PORT_NUMBER_1;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_answertype);

//		Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskSensor_Port_1 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_answertype, length_arr);
		
#if LOG_CREATE_FRAME
	Sec_Log_Status("Create_Frame_AskSensor_Port_1 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskSensor_Port_2(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
	frame_sensor_test.PortNumber  = PORT_NUMBER_2;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

//		Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskSensor_Port_2 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_asktype_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskSensor_Port_2 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskSensor_Port_3(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
	frame_sensor_test.PortNumber  = PORT_NUMBER_3;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

//		Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskSensor_Port_3 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_asktype_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskSensor_Port_3 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskSensor_Port_4(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
	frame_sensor_test.PortNumber  = PORT_NUMBER_4;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

//		Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskSensor_Port_4 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_asktype_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskSensor_Port_4 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskSensor_Port_5(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
	frame_sensor_test.PortNumber  = PORT_NUMBER_5;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

//		Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskSensor_Port_5 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_asktype_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskSensor_Port_5 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskSensor_Port_6(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
	frame_sensor_test.PortNumber  = PORT_NUMBER_6;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

//		Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskSensor_Port_6 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_asktype_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskSensor_Port_6 - SendDone");
#endif
		
	return 1;
}

/*---------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------- (- Data Sensor -) ----------------------------------------------*/
uint8_t Create_Frame_AskData_Port_1(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
	frame_sensor_test.PortNumber  = PORT_NUMBER_1;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

//		Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskData_Port_1 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_askdata_out, length_arr);
		
#if LOG_CREATE_FRAME
	Sec_Log_Status("Create_Frame_AskData_Port_1 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskData_Port_2(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
	frame_sensor_test.PortNumber  = PORT_NUMBER_2;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

//		Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskData_Port_2 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_askdata_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskData_Port_2 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskData_Port_3(void)
{	
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
	frame_sensor_test.PortNumber  = PORT_NUMBER_3;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

//		Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskData_Port_3 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_askdata_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskData_Port_3 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskData_Port_4(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
	frame_sensor_test.PortNumber  = PORT_NUMBER_4;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

//		Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskData_Port_4 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_askdata_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskData_Port_4 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskData_Port_5(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
	frame_sensor_test.PortNumber  = PORT_NUMBER_5;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

//		Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskData_Port_5 - CreateError");
		return 0;
	}

	SEC_Uart2_SendByte(arr_data_sensor_create_frame_askdata_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskData_Port_5 - SendDone");
#endif
		
	return 1;
}

uint8_t Create_Frame_AskData_Port_6(void)
{
	sec_FrameMsg_t frame_sensor_test;
	uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;
	
	frame_sensor_test.StartFrame  = START_BYTE;
	frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
	frame_sensor_test.PortNumber  = PORT_NUMBER_6;
	length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

//		Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
	
	if(length_arr < 0)
	{
		Sec_Log_Error("Create_Frame_AskData_Port_6 - CreateError");
		return 0;
	}
	
	SEC_Uart2_SendByte(arr_data_sensor_create_frame_askdata_out, length_arr);
		
#if LOG_CREATE_FRAME
		Sec_Log_Status("Create_Frame_AskData_Port_6 - SendDone");
#endif
		
	return 1;
}




/*------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------*/

void Print_Data_Message(uint8_t *arr, uint8_t length)
{
    printf("- Create -\n");

    for (uint8_t i = 0; i < length; i++)
    {
        if (arr[i] <= 0x0F)
        {
            printf("0%x ", arr[i]);
        }
        else
        {
            printf("%x ", arr[i]);
        }
    }
	
	printf("\n");
}

/*------------------------------------------------------------------------------------------------------------*/

void Print_Data_Detect(sec_FrameMsg_t frame)
{
	float data_float = 0;
	float temperature = 0, humidity = 0;
	
    printf("------------- Detect -------------");
//    printf("\n Header       : %x ", frame.StartFrame);
//    printf("\n TypeMessage  : %x ", frame.TypeMessage);
    printf("\n PortNumber   : %x ", frame.PortNumber);
//    printf("\n LengthData   : %d ", frame.LengthData);
    
    switch (frame.TypeMessage)
    {
        case TYPE_MESSAGE_ANSWER_TYPE:
//            printf("\n TypeSensor   : %x ", frame.Data[0]);
        break;

        case TYPE_MESSAGE_ANSWER_DATA:
//            printf("\n TypeSensor   : %x ", frame.Data[0]);

            switch (frame.Data[0])
            {
                case SENSOR_SHT30:
					// nhiet do
                    temperature = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n Temperature  : %0.2f ", temperature);
					// do am
					humidity = Sec_Convert_From_Bytes_To_Float(frame.Data[5], frame.Data[6], frame.Data[7], frame.Data[8]);
                    printf("\n Humidity     : %0.2f ", humidity);
                break;
				
				case SENSOR_NTC:
                    temperature = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n Temperature  : %0.2f ", temperature);
                break;

                case SENSOR_3:
                    humidity = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n humidity    : %0.2f ", humidity);
                break;

                case SENSOR_4:
                    data_float = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n DataSensor   : %0.2f ", data_float);
                break;

                case SENSOR_5:
                    data_float = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n DataSensor   : %0.2f ", data_float);
                break;

                case SENSOR_6:
                    data_float = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n DataSensor   : %0.2f ", data_float);
                break;
            }
        break;
    }

//    printf("\n CheckFrame   : %x\n", frame.CheckFrame);
	printf("\n");
}

/*------------------------------------------------------------------------------------------------------------*/
