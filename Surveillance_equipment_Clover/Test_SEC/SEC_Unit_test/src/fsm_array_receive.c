#include "fsm_array_receive.h"


fsm_state_e fsm_state = FSM_STATE_START_FRAME;
// uint8_t array_out[50];
uint8_t fsm_count_array = 0;
// uint8_t count_array_in = 0;
uint8_t fsm_data_length;

uint8_t Fsm_Test_Array_Receive(uint8_t array_in, uint8_t *array_out)
{
    uint8_t temp_data = array_in;

    switch (fsm_state)
    {
        case FSM_STATE_START_FRAME:
            array_out[fsm_count_array] = temp_data;
            
            fsm_count_array++;

            if(fsm_count_array == FSM_STATE_CHANGE_VALUE_TYPE_MESSAGE)
            {
                fsm_state = FSM_STATE_TYPE_MESSAGE;
            }
        break;
        
        case FSM_STATE_TYPE_MESSAGE:
            array_out[fsm_count_array] = temp_data;
            
            fsm_count_array++;

            if(fsm_count_array == FSM_STATE_CHANGE_VALUE_PORT_NUMBER)
            {
                fsm_state = FSM_STATE_PORT_NUMBER;
            }
        break;

        case FSM_STATE_PORT_NUMBER:
            array_out[fsm_count_array] = temp_data;
            
            fsm_count_array++;

            if(fsm_count_array == FSM_STATE_CHANGE_VALUE_LENGHT_DATA)
            {
                fsm_state = FSM_STATE_LENGHT_DATA;
            }
        break;

        case FSM_STATE_LENGHT_DATA:
            array_out[fsm_count_array] = temp_data;
            
            fsm_count_array++;

            if(fsm_count_array == FSM_STATE_CHANGE_VALUE_END)
            {
                fsm_data_length = Sec_Convert_From_Bytes_To_Uint16(array_out[4], array_out[5]) + 6;

                if(fsm_data_length < 24)
                {
                    fsm_state = FSM_STATE_END;
                }
                else if(fsm_data_length > 24)
                {
                    fsm_count_array = 0;
                    fsm_state = FSM_STATE_START_FRAME;
                }
            }
        break;

        case FSM_STATE_END:
            array_out[fsm_count_array] = temp_data;
            
            fsm_count_array++;

            if(fsm_count_array == fsm_data_length)
            {
                fsm_count_array = 0;
                fsm_state = FSM_STATE_START_FRAME;
                return 1;
            }
        break;
    }
    return 0; 
}