#ifndef FSM_ARRAY_RECEIVE_H
#define FSM_ARRAY_RECEIVE_H

#include "stdint.h"
#include "sec_convert.h"
#include "sec_message.h"

// extern uint8_t array_out[50];

#define FSM_STATE 5 
typedef enum
{
    FSM_STATE_START_FRAME  = 0,
    FSM_STATE_TYPE_MESSAGE = 1,
    FSM_STATE_PORT_NUMBER  = 2,
    FSM_STATE_LENGHT_DATA  = 3,
    FSM_STATE_END          = 4,
} fsm_state_e;


typedef enum
{
    FSM_STATE_CHANGE_VALUE_START_FRAME  = 0,
    FSM_STATE_CHANGE_VALUE_TYPE_MESSAGE = 2,
    FSM_STATE_CHANGE_VALUE_PORT_NUMBER  = 3,
    FSM_STATE_CHANGE_VALUE_LENGHT_DATA  = 4,
    FSM_STATE_CHANGE_VALUE_END          = 6,
} fsm_state_change_value_e;


uint8_t Fsm_Test_Array_Receive(uint8_t array_in, uint8_t *array_out);

#endif // FSM_ARRAY_RECEIVE_H
