#include "src/fsm_array_receive.h"
#include "src/sec_message.h"
#include "src/sec_convert.h"
#include "/var/lib/gems/3.0.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"






void setUp(void)

{

}



void tearDown(void)

{

}



void Print_Data_Message(uint8_t *arr, uint8_t length);

void Print_Data_Detect(sec_FrameMsg_t frame);









void test_SEC_Message_Create_Frame_Asktype_Port_1(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_asktype_test[4 + 2] =

    {0x55,0xAA,0x01,0x00,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_1\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(45), UNITY_DISPLAY_STYLE_UINT8);

    }









    void Print_Data_Message_Fsm(uint8_t *arr, uint8_t length);



    count_arr_test = 0;

    uint8_t arr_data_fsm[50];

    while (1)

    {

        if (Fsm_Test_Array_Receive(arr_data_sensor_create_frame_asktype_out[count_arr_test++], arr_data_fsm))

        {

            break;

        }

    }

    for (count_arr_test = 0; count_arr_test < 8 ; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_fsm[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(64), UNITY_DISPLAY_STYLE_UINT8);

    }

    Print_Data_Message_Fsm(arr_data_fsm, length_arr);











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[4],

                                                                    arr_data_sensor_create_frame_asktype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(80), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(81), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(82), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(83), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(84), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_1\n");

}



void test_SEC_Message_Create_Frame_Asktype_Port_2(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_asktype_test[4 + 2] =

    {0x55,0xAA,0x01,0x01,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_2;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_2\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(116), UNITY_DISPLAY_STYLE_UINT8);

    }









    void Print_Data_Message_Fsm(uint8_t *arr, uint8_t length);



    count_arr_test = 0;

    uint8_t arr_data_fsm[50];

    while (1)

    {

        if (Fsm_Test_Array_Receive(arr_data_sensor_create_frame_asktype_out[count_arr_test++], arr_data_fsm))

        {

            break;

        }

    }

    for (count_arr_test = 0; count_arr_test < 8 ; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_fsm[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(135), UNITY_DISPLAY_STYLE_UINT8);

    }

    Print_Data_Message_Fsm(arr_data_fsm, length_arr);











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[4],

                                                                    arr_data_sensor_create_frame_asktype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(151), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(152), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(153), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(154), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(155), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_2\n");

}



void test_SEC_Message_Create_Frame_Asktype_Port_3(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_asktype_test[4 + 2] =

    {0x55,0xAA,0x01,0x02,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_3;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_3\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(187), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[4],

                                                                    arr_data_sensor_create_frame_asktype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(202), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(203), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(204), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(205), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(206), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_3\n");

}



void test_SEC_Message_Create_Frame_Asktype_Port_4(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_asktype_test[4 + 2] =

    {0x55,0xAA,0x01,0x03,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_4;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_4\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(238), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[4],

                                                                    arr_data_sensor_create_frame_asktype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(253), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(254), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(255), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(256), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(257), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_4\n");

}



void test_SEC_Message_Create_Frame_Asktype_Port_5(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_asktype_test[4 + 2] =

    {0x55,0xAA,0x01,0x04,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_5;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_5\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(289), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[4],

                                                                    arr_data_sensor_create_frame_asktype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(304), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(305), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(306), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(307), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(308), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_5\n");

}



void test_SEC_Message_Create_Frame_Asktype_Port_6(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_asktype_test[4 + 2] =

    {0x55,0xAA,0x01,0x05,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_6;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_6\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_asktype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(340), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[4],

                                                                    arr_data_sensor_create_frame_asktype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(355), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(356), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(357), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(358), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(359), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_6\n");

}



void test_SEC_Message_Create_Frame_Answertype_Sensor_1(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_answertype_test[4 + 3] =

    {0x55,0xAA,0x02,0x00,0x03,0x00,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_1;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_1\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(392), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[4],

                                                                    arr_data_sensor_create_frame_answertype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(407), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(408), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(409), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(410), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(411), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(412), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_1\n");

}



void test_SEC_Message_Create_Frame_Answertype_Sensor_2(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_answertype_test[4 + 3] =

    {0x55,0xAA,0x02,0x00,0x03,0x00,0x01};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_2;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_2\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(445), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[4],

                                                                    arr_data_sensor_create_frame_answertype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(460), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(461), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(462), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(463), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(464), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(465), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_2\n");

}



void test_SEC_Message_Create_Frame_Answertype_Sensor_3(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_answertype_test[4 + 3] =

    {0x55,0xAA,0x02,0x00,0x03,0x00,0x02};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_3;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_3\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(498), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[4],

                                                                    arr_data_sensor_create_frame_answertype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(513), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(514), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(515), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(516), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(517), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(518), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_3\n");

}



void test_SEC_Message_Create_Frame_Answertype_Sensor_4(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_answertype_test[4 + 3] =

    {0x55,0xAA,0x02,0x00,0x03,0x00,0x03};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_4;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_4\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(551), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[4],

                                                                    arr_data_sensor_create_frame_answertype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(566), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(567), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(568), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(569), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(570), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(571), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_4\n");

}



void test_SEC_Message_Create_Frame_Answertype_Sensor_5(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_answertype_test[4 + 3] =

    {0x55,0xAA,0x02,0x00,0x03,0x00,0x04};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_5;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_5\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(604), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[4],

                                                                    arr_data_sensor_create_frame_answertype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(619), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(620), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(621), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(622), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(623), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(624), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_5\n");

}



void test_SEC_Message_Create_Frame_Answertype_Sensor_6(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_answertype_test[4 + 3] =

    {0x55,0xAA,0x02,0x00,0x03,0x00,0x05};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_6;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_6\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answertype_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(657), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[4],

                                                                    arr_data_sensor_create_frame_answertype_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(672), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(673), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(674), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(675), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(676), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(677), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_6\n");

}









void test_SEC_Message_Create_Frame_Askdata(void)

{





    uint8_t count_arr_test = 0;

    uint8_t arr_data_sensor_create_frame_askdata_test[4 + 2] =

    {0x55,0xAA,0x03,0x00,0x02,0x00};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Askdata\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 2; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_askdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_askdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(712), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_askdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_askdata_out[4],

                                                                    arr_data_sensor_create_frame_askdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_askdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_askdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(727), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(728), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(729), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(730), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(731), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Askdata\n");

}



void test_SEC_Message_Create_Frame_Answerdata_Sensor_1(void)

{





    float test_sec_message_data = 22.5;

    uint8_t count_arr_test = 0;

    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);

    uint8_t arr_data_sensor_create_frame_answerdata_test[4 + 3 + 4] =

    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x00, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_1;

    frame_sensor_test.Data[1] = data_float_to_byte[3];

    frame_sensor_test.Data[2] = data_float_to_byte[2];

    frame_sensor_test.Data[3] = data_float_to_byte[1];

    frame_sensor_test.Data[4] = data_float_to_byte[0];



    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_1\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3 + 4; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(771), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[4],

                                                                    arr_data_sensor_create_frame_answerdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(786), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(787), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(788), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(789), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(790), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[1])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[1])), (

   ((void *)0)

   ), (UNITY_UINT)(791), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[2])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[2])), (

   ((void *)0)

   ), (UNITY_UINT)(792), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[3])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[3])), (

   ((void *)0)

   ), (UNITY_UINT)(793), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[4])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[4])), (

   ((void *)0)

   ), (UNITY_UINT)(794), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(795), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_1\n");

}



void test_SEC_Message_Create_Frame_Answerdata_Sensor_2(void)

{





    float test_sec_message_data = 23.5;

    uint8_t count_arr_test = 0;

    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);

    uint8_t arr_data_sensor_create_frame_answerdata_test[4 + 3 + 4] =

    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x01, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_2;

    frame_sensor_test.Data[1] = data_float_to_byte[3];

    frame_sensor_test.Data[2] = data_float_to_byte[2];

    frame_sensor_test.Data[3] = data_float_to_byte[1];

    frame_sensor_test.Data[4] = data_float_to_byte[0];



    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_2\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3 + 4; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(835), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[4],

                                                                    arr_data_sensor_create_frame_answerdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(850), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(851), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(852), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(853), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(854), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[1])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[1])), (

   ((void *)0)

   ), (UNITY_UINT)(855), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[2])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[2])), (

   ((void *)0)

   ), (UNITY_UINT)(856), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[3])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[3])), (

   ((void *)0)

   ), (UNITY_UINT)(857), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[4])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[4])), (

   ((void *)0)

   ), (UNITY_UINT)(858), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(859), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_2\n");

}



void test_SEC_Message_Create_Frame_Answerdata_Sensor_3(void)

{





    float test_sec_message_data = 24.5;

    uint8_t count_arr_test = 0;

    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);

    uint8_t arr_data_sensor_create_frame_answerdata_test[4 + 3 + 4] =

    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x02, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_3;

    frame_sensor_test.Data[1] = data_float_to_byte[3];

    frame_sensor_test.Data[2] = data_float_to_byte[2];

    frame_sensor_test.Data[3] = data_float_to_byte[1];

    frame_sensor_test.Data[4] = data_float_to_byte[0];



    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_3\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3 + 4; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(899), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[4],

                                                                    arr_data_sensor_create_frame_answerdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(914), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(915), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(916), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(917), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(918), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[1])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[1])), (

   ((void *)0)

   ), (UNITY_UINT)(919), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[2])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[2])), (

   ((void *)0)

   ), (UNITY_UINT)(920), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[3])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[3])), (

   ((void *)0)

   ), (UNITY_UINT)(921), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[4])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[4])), (

   ((void *)0)

   ), (UNITY_UINT)(922), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(923), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_3\n");

}



void test_SEC_Message_Create_Frame_Answerdata_Sensor_4(void)

{





    float test_sec_message_data = 25.5;

    uint8_t count_arr_test = 0;

    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);

    uint8_t arr_data_sensor_create_frame_answerdata_test[4 + 3 + 4] =

    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x03, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_4;

    frame_sensor_test.Data[1] = data_float_to_byte[3];

    frame_sensor_test.Data[2] = data_float_to_byte[2];

    frame_sensor_test.Data[3] = data_float_to_byte[1];

    frame_sensor_test.Data[4] = data_float_to_byte[0];



    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_4\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3 + 4; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(963), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[4],

                                                                    arr_data_sensor_create_frame_answerdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(978), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(979), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(980), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(981), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(982), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[1])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[1])), (

   ((void *)0)

   ), (UNITY_UINT)(983), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[2])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[2])), (

   ((void *)0)

   ), (UNITY_UINT)(984), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[3])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[3])), (

   ((void *)0)

   ), (UNITY_UINT)(985), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[4])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[4])), (

   ((void *)0)

   ), (UNITY_UINT)(986), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(987), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_4\n");

}



void test_SEC_Message_Create_Frame_Answerdata_Sensor_5(void)

{





    float test_sec_message_data = 26.5;

    uint8_t count_arr_test = 0;

    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);

    uint8_t arr_data_sensor_create_frame_answerdata_test[4 + 3 + 4] =

    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x04, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_5;

    frame_sensor_test.Data[1] = data_float_to_byte[3];

    frame_sensor_test.Data[2] = data_float_to_byte[2];

    frame_sensor_test.Data[3] = data_float_to_byte[1];

    frame_sensor_test.Data[4] = data_float_to_byte[0];



    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/");

    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_5\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3 + 4; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(1027), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[4],

                                                                    arr_data_sensor_create_frame_answerdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(1042), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(1043), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(1044), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(1045), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(1046), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[1])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[1])), (

   ((void *)0)

   ), (UNITY_UINT)(1047), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[2])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[2])), (

   ((void *)0)

   ), (UNITY_UINT)(1048), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[3])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[3])), (

   ((void *)0)

   ), (UNITY_UINT)(1049), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[4])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[4])), (

   ((void *)0)

   ), (UNITY_UINT)(1050), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(1051), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_5\n");

}



void test_SEC_Message_Create_Frame_Answerdata_Sensor_6(void)

{





    float test_sec_message_data = 27.5;

    uint8_t count_arr_test = 0;

    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);

    uint8_t arr_data_sensor_create_frame_answerdata_test[4 + 3 + 4] =

    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x05, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};



    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;

    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;



    frame_sensor_test.StartFrame = 0xAA55;

    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;

    frame_sensor_test.PortNumber = PORT_NUMBER_1;

    frame_sensor_test.Data[0] = SENSOR_6;

    frame_sensor_test.Data[1] = data_float_to_byte[3];

    frame_sensor_test.Data[2] = data_float_to_byte[2];

    frame_sensor_test.Data[3] = data_float_to_byte[1];

    frame_sensor_test.Data[4] = data_float_to_byte[0];



    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);



    printf("/*-------------------------------------------------------------------------------------------------------*/\n");

    printf("Start test_SEC_Message_Create_Frame_Answerdata_Sensor_6\n");

    printf("length: %d\n", length_arr);

    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);



    for (count_arr_test = 0; count_arr_test < 4 + 3 + 4; count_arr_test++)

    {

        UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_test[count_arr_test])), (UNITY_INT)(UNITY_UINT8 )((arr_data_sensor_create_frame_answerdata_out[count_arr_test])), (

       ((void *)0)

       ), (UNITY_UINT)(1091), UNITY_DISPLAY_STYLE_UINT8);

    }











    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);



    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[4],

                                                                    arr_data_sensor_create_frame_answerdata_out[4 + 1]);



    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],

                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.StartFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.StartFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(1106), UNITY_DISPLAY_STYLE_HEX16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.TypeMessage)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.TypeMessage)), (

   ((void *)0)

   ), (UNITY_UINT)(1107), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.PortNumber)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.PortNumber)), (

   ((void *)0)

   ), (UNITY_UINT)(1108), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.LengthData)), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.LengthData)), (

   ((void *)0)

   ), (UNITY_UINT)(1109), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[0])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[0])), (

   ((void *)0)

   ), (UNITY_UINT)(1110), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[1])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[1])), (

   ((void *)0)

   ), (UNITY_UINT)(1111), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[2])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[2])), (

   ((void *)0)

   ), (UNITY_UINT)(1112), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[3])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[3])), (

   ((void *)0)

   ), (UNITY_UINT)(1113), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT8 )((frame_sensor_test.Data[4])), (UNITY_INT)(UNITY_INT8 )((frame_sensor_detect.Data[4])), (

   ((void *)0)

   ), (UNITY_UINT)(1114), UNITY_DISPLAY_STYLE_HEX8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((frame_sensor_test.CheckFrame)), (UNITY_INT)(UNITY_INT16)((frame_sensor_detect.CheckFrame)), (

   ((void *)0)

   ), (UNITY_UINT)(1115), UNITY_DISPLAY_STYLE_HEX16);



    Print_Data_Detect(frame_sensor_detect);







    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_6\n");

}







void Print_Data_Message(uint8_t *arr, uint8_t length)

{

    printf("- Create -\n");



    for (uint8_t i = 0; i < length; i++)

    {

        if (arr[i] <= 0x0F)

        {

            printf("0%x ", arr[i]);

        }

        else

        {

            printf("%x ", arr[i]);

        }

    }

}





void Print_Data_Detect(sec_FrameMsg_t frame)

{

    printf("\n- Detect -");

    printf("\n Header       : %x ", frame.StartFrame);

    printf("\n TypeMessage  : %x ", frame.TypeMessage);

    printf("\n PortNumber   : %x ", frame.PortNumber);

    printf("\n LengthData   : %x ", frame.LengthData);



    switch (frame.TypeMessage)

    {

        case TYPE_MESSAGE_ANSWER_TYPE:

            printf("\n TypeSensor   : %x ", frame.Data[0]);

        break;



        case TYPE_MESSAGE_ANSWER_DATA:

            printf("\n TypeSensor   : %x ", frame.Data[0]);



            switch (frame.Data[0])

            {

                case SENSOR_1: case SENSOR_2: case SENSOR_3: case SENSOR_4: case SENSOR_5: case SENSOR_6:

                    float data_float = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);

                    printf("\n DataSensor : %f ", data_float);

                break;

            }

        break;

    }



    printf("\n CheckFrame   : %x ", frame.CheckFrame);

}







void Print_Data_Message_Fsm(uint8_t *arr, uint8_t length)

{

    printf("\nGi tr phn t trong mng c truyn qua USART: ");

    for (uint8_t i = 0; i < length; i++)

    {

        if (arr[i] <= 0x0F)

        {

            printf("0%x ", arr[i]);

        }

        else

        {

            printf("%x ", arr[i]);

        }

    }

}
