// #ifdef TEST

#include "unity.h"
#include "sec_convert.h"
#include "sec_message.h"
#include "fsm_array_receive.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void Print_Data_Message(uint8_t *arr, uint8_t length);
void Print_Data_Detect(sec_FrameMsg_t frame);


/*---------------------------------------------------- TYPE --------------------------------------------------------------*/

void test_SEC_Message_Create_Frame_Asktype_Port_1(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_AskType)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_asktype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE] = 
    {0x55,0xAA,0x01,0x00,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_1\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_test[count_arr_test], arr_data_sensor_create_frame_asktype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_AskType)--------------------------------------*/

    /*------------------------------------------------------ TEST - FSM ------------------------------------------------*/
    void Print_Data_Message_Fsm(uint8_t *arr, uint8_t length);

    count_arr_test = 0;
    uint8_t arr_data_fsm[50];
    while (1)
    {
        if (Fsm_Test_Array_Receive(arr_data_sensor_create_frame_asktype_out[count_arr_test++], arr_data_fsm))
        {
            break;
        }
    }
    for (count_arr_test = 0; count_arr_test < 8 ; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_out[count_arr_test],arr_data_fsm[count_arr_test]);
    }
    Print_Data_Message_Fsm(arr_data_fsm, length_arr);

    /*------------------------------------------------------ TEST - FSM ------------------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_AskType)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_AskType)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_1\n");
}

void test_SEC_Message_Create_Frame_Asktype_Port_2(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_AskType)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_asktype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE] = 
    {0x55,0xAA,0x01,0x01,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_2;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_2\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_test[count_arr_test], arr_data_sensor_create_frame_asktype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_AskType)--------------------------------------*/

    /*------------------------------------------------------ TEST - FSM ------------------------------------------------*/
    void Print_Data_Message_Fsm(uint8_t *arr, uint8_t length);

    count_arr_test = 0;
    uint8_t arr_data_fsm[50];
    while (1)
    {
        if (Fsm_Test_Array_Receive(arr_data_sensor_create_frame_asktype_out[count_arr_test++], arr_data_fsm))
        {
            break;
        }
    }
    for (count_arr_test = 0; count_arr_test < 8 ; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_out[count_arr_test],arr_data_fsm[count_arr_test]);
    }
    Print_Data_Message_Fsm(arr_data_fsm, length_arr);

    /*------------------------------------------------------ TEST - FSM ------------------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_AskType)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_AskType)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_2\n");
}

void test_SEC_Message_Create_Frame_Asktype_Port_3(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_AskType)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_asktype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE] = 
    {0x55,0xAA,0x01,0x02,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_3;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_3\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_test[count_arr_test], arr_data_sensor_create_frame_asktype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_AskType)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_AskType)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_AskType)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_3\n");
}

void test_SEC_Message_Create_Frame_Asktype_Port_4(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_AskType)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_asktype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE] = 
    {0x55,0xAA,0x01,0x03,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_4;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_4\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_test[count_arr_test], arr_data_sensor_create_frame_asktype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_AskType)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_AskType)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_AskType)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_4\n");
}

void test_SEC_Message_Create_Frame_Asktype_Port_5(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_AskType)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_asktype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE] = 
    {0x55,0xAA,0x01,0x04,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_5;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_5\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_test[count_arr_test], arr_data_sensor_create_frame_asktype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_AskType)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_AskType)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_AskType)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_5\n");
}

void test_SEC_Message_Create_Frame_Asktype_Port_6(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_AskType)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_asktype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE] = 
    {0x55,0xAA,0x01,0x05,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_asktype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_6;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_asktype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Asktype_Port_6\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_asktype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_asktype_test[count_arr_test], arr_data_sensor_create_frame_asktype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_AskType)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_AskType)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_asktype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_asktype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_asktype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_asktype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_AskType)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Asktype_Port_6\n");
}

void test_SEC_Message_Create_Frame_Answertype_Sensor_1(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_answertype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE] = 
    {0x55,0xAA,0x02,0x00,0x03,0x00,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_1;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_1\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answertype_test[count_arr_test], arr_data_sensor_create_frame_answertype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERTYPE)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]);  // TypeSensor
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ANSWERTYPE)----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_1\n");
}

void test_SEC_Message_Create_Frame_Answertype_Sensor_2(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_answertype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE] = 
    {0x55,0xAA,0x02,0x00,0x03,0x00,0x01};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_2;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_2\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answertype_test[count_arr_test], arr_data_sensor_create_frame_answertype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERTYPE)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]);  // TypeSensor
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ANSWERTYPE)----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_2\n");
}

void test_SEC_Message_Create_Frame_Answertype_Sensor_3(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_answertype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE] = 
    {0x55,0xAA,0x02,0x00,0x03,0x00,0x02};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_3;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_3\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answertype_test[count_arr_test], arr_data_sensor_create_frame_answertype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERTYPE)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]);  // TypeSensor
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ANSWERTYPE)----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_3\n");
}

void test_SEC_Message_Create_Frame_Answertype_Sensor_4(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_answertype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE] = 
    {0x55,0xAA,0x02,0x00,0x03,0x00,0x03};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_4;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_4\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answertype_test[count_arr_test], arr_data_sensor_create_frame_answertype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERTYPE)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]);  // TypeSensor
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ANSWERTYPE)----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_4\n");
}

void test_SEC_Message_Create_Frame_Answertype_Sensor_5(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_answertype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE] = 
    {0x55,0xAA,0x02,0x00,0x03,0x00,0x04};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_5;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_5\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answertype_test[count_arr_test], arr_data_sensor_create_frame_answertype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERTYPE)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]);  // TypeSensor
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ANSWERTYPE)----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_5\n");
}

void test_SEC_Message_Create_Frame_Answertype_Sensor_6(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_answertype_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE] = 
    {0x55,0xAA,0x02,0x00,0x03,0x00,0x05};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answertype_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_TYPE;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_6;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answertype_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answertype_Sensor_6\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answertype_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERTYPE; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answertype_test[count_arr_test], arr_data_sensor_create_frame_answertype_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERTYPE)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERTYPE)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answertype_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answertype_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answertype_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answertype_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]);  // TypeSensor
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ANSWERTYPE)----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answertype_Sensor_6\n");
}


/*---------------------------------------------------- DATA --------------------------------------------------------------*/

void test_SEC_Message_Create_Frame_Askdata(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ASKDATA)--------------------------------------*/
    
    uint8_t count_arr_test = 0;
    uint8_t arr_data_sensor_create_frame_askdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ASKDATA] = 
    {0x55,0xAA,0x03,0x00,0x02,0x00};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_askdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ASK_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_askdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Askdata\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_askdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ASKDATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_askdata_test[count_arr_test], arr_data_sensor_create_frame_askdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ASKDATA)--------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ASKDATA)--------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_askdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_askdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_askdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_askdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_askdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*------------------------------------------------(END_DETECT_FRAME_ASKDATA)--------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Askdata\n");
}

void test_SEC_Message_Create_Frame_Answerdata_Sensor_1(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERDATA)--------------------------------------*/

    float test_sec_message_data = 22.5;
    uint8_t count_arr_test      = 0;
    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);
    uint8_t arr_data_sensor_create_frame_answerdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_1_DATA] = 
    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x00, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_1;
    frame_sensor_test.Data[1]     = data_float_to_byte[3];
    frame_sensor_test.Data[2]     = data_float_to_byte[2];
    frame_sensor_test.Data[3]     = data_float_to_byte[1];
    frame_sensor_test.Data[4]     = data_float_to_byte[0];

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_1\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_1_DATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answerdata_test[count_arr_test], arr_data_sensor_create_frame_answerdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERDATA)----------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERDATA)----------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]); // TypeSensor
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[1], frame_sensor_detect.Data[1]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[2], frame_sensor_detect.Data[2]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[3], frame_sensor_detect.Data[3]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[4], frame_sensor_detect.Data[4]);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*-------------------------------------------------(END_DETECT_FRAME_ANSWERDATA)-----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_1\n");
}

void test_SEC_Message_Create_Frame_Answerdata_Sensor_2(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERDATA)--------------------------------------*/

    float test_sec_message_data = 23.5;
    uint8_t count_arr_test      = 0;
    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);
    uint8_t arr_data_sensor_create_frame_answerdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_2_DATA] = 
    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x01, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_2;
    frame_sensor_test.Data[1]     = data_float_to_byte[3];
    frame_sensor_test.Data[2]     = data_float_to_byte[2];
    frame_sensor_test.Data[3]     = data_float_to_byte[1];
    frame_sensor_test.Data[4]     = data_float_to_byte[0];

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_2\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_2_DATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answerdata_test[count_arr_test], arr_data_sensor_create_frame_answerdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERDATA)----------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERDATA)----------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]); // TypeSensor
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[1], frame_sensor_detect.Data[1]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[2], frame_sensor_detect.Data[2]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[3], frame_sensor_detect.Data[3]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[4], frame_sensor_detect.Data[4]);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*-------------------------------------------------(END_DETECT_FRAME_ANSWERDATA)-----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_2\n");
}

void test_SEC_Message_Create_Frame_Answerdata_Sensor_3(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERDATA)--------------------------------------*/

    float test_sec_message_data = 24.5;
    uint8_t count_arr_test      = 0;
    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);
    uint8_t arr_data_sensor_create_frame_answerdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_3_DATA] = 
    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x02, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_3;
    frame_sensor_test.Data[1]     = data_float_to_byte[3];
    frame_sensor_test.Data[2]     = data_float_to_byte[2];
    frame_sensor_test.Data[3]     = data_float_to_byte[1];
    frame_sensor_test.Data[4]     = data_float_to_byte[0];

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_3\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_3_DATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answerdata_test[count_arr_test], arr_data_sensor_create_frame_answerdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERDATA)----------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERDATA)----------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]); // TypeSensor
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[1], frame_sensor_detect.Data[1]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[2], frame_sensor_detect.Data[2]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[3], frame_sensor_detect.Data[3]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[4], frame_sensor_detect.Data[4]);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*-------------------------------------------------(END_DETECT_FRAME_ANSWERDATA)-----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_3\n");
}

void test_SEC_Message_Create_Frame_Answerdata_Sensor_4(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERDATA)--------------------------------------*/

    float test_sec_message_data = 25.5;
    uint8_t count_arr_test      = 0;
    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);
    uint8_t arr_data_sensor_create_frame_answerdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_4_DATA] = 
    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x03, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_4;
    frame_sensor_test.Data[1]     = data_float_to_byte[3];
    frame_sensor_test.Data[2]     = data_float_to_byte[2];
    frame_sensor_test.Data[3]     = data_float_to_byte[1];
    frame_sensor_test.Data[4]     = data_float_to_byte[0];

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_4\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_4_DATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answerdata_test[count_arr_test], arr_data_sensor_create_frame_answerdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERDATA)----------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERDATA)----------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]); // TypeSensor
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[1], frame_sensor_detect.Data[1]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[2], frame_sensor_detect.Data[2]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[3], frame_sensor_detect.Data[3]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[4], frame_sensor_detect.Data[4]);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*-------------------------------------------------(END_DETECT_FRAME_ANSWERDATA)-----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_4\n");
}

void test_SEC_Message_Create_Frame_Answerdata_Sensor_5(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERDATA)--------------------------------------*/

    float test_sec_message_data = 26.5;
    uint8_t count_arr_test      = 0;
    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);
    uint8_t arr_data_sensor_create_frame_answerdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_5_DATA] = 
    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x04, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_5;
    frame_sensor_test.Data[1]     = data_float_to_byte[3];
    frame_sensor_test.Data[2]     = data_float_to_byte[2];
    frame_sensor_test.Data[3]     = data_float_to_byte[1];
    frame_sensor_test.Data[4]     = data_float_to_byte[0];

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/");
    printf("\nStart test_SEC_Message_Create_Frame_Answerdata_Sensor_5\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_5_DATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answerdata_test[count_arr_test], arr_data_sensor_create_frame_answerdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERDATA)----------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERDATA)----------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]); // TypeSensor
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[1], frame_sensor_detect.Data[1]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[2], frame_sensor_detect.Data[2]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[3], frame_sensor_detect.Data[3]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[4], frame_sensor_detect.Data[4]);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*-------------------------------------------------(END_DETECT_FRAME_ANSWERDATA)-----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_5\n");
}

void test_SEC_Message_Create_Frame_Answerdata_Sensor_6(void)
{
    /*---------------------------------------------------(START_CREAT_FRAME_ANSWERDATA)--------------------------------------*/

    float test_sec_message_data = 27.5;
    uint8_t count_arr_test      = 0;
    uint8_t *data_float_to_byte = Sec_Convert_From_Float_To_Bytes(test_sec_message_data);
    uint8_t arr_data_sensor_create_frame_answerdata_test[LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_6_DATA] = 
    {0x55, 0xAA, 0x04, 0x00, 0x07, 0x00, 0x05, data_float_to_byte[3], data_float_to_byte[2], data_float_to_byte[1], data_float_to_byte[0]};

    sec_FrameMsg_t frame_sensor_test, frame_sensor_detect;
    uint8_t arr_data_sensor_create_frame_answerdata_out[50], length_arr = 0;

    frame_sensor_test.StartFrame  = START_BYTE;
    frame_sensor_test.TypeMessage = TYPE_MESSAGE_ANSWER_DATA;
    frame_sensor_test.PortNumber  = PORT_NUMBER_1;
    frame_sensor_test.Data[0]     = SENSOR_6;
    frame_sensor_test.Data[1]     = data_float_to_byte[3];
    frame_sensor_test.Data[2]     = data_float_to_byte[2];
    frame_sensor_test.Data[3]     = data_float_to_byte[1];
    frame_sensor_test.Data[4]     = data_float_to_byte[0];

    length_arr = SEC_Message_Create_Frame(frame_sensor_test, arr_data_sensor_create_frame_answerdata_out);

    printf("/*-------------------------------------------------------------------------------------------------------*/\n");
    printf("Start test_SEC_Message_Create_Frame_Answerdata_Sensor_6\n");
    printf("length: %d\n", length_arr);
    Print_Data_Message(arr_data_sensor_create_frame_answerdata_out, length_arr);
    // tạo bản tin dạng frame từ arr
    for (count_arr_test = 0; count_arr_test < LENGTH_DEFAULT + LENGTH_SENSOR_ANSWERDATA + LENGTH_SENSOR_6_DATA; count_arr_test++)
    {
        TEST_ASSERT_EQUAL_UINT8(arr_data_sensor_create_frame_answerdata_test[count_arr_test], arr_data_sensor_create_frame_answerdata_out[count_arr_test]);
    }

    /*---------------------------------------------------(END_CREAT_FRAME_ANSWERDATA)----------------------------------------*/

    /*------------------------------------------------(START_DETECT_FRAME_ANSWERDATA)----------------------------------------*/

    SEC_Message_Detect_Frame(arr_data_sensor_create_frame_answerdata_out, &frame_sensor_detect);

    frame_sensor_test.LengthData = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT],
                                                                    arr_data_sensor_create_frame_answerdata_out[LENGTH_DEFAULT + 1]);

    frame_sensor_test.CheckFrame = Sec_Convert_From_Bytes_To_Uint16(arr_data_sensor_create_frame_answerdata_out[length_arr - 2],
                                                                    arr_data_sensor_create_frame_answerdata_out[length_arr - 1]);

    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.StartFrame, frame_sensor_detect.StartFrame);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.TypeMessage, frame_sensor_detect.TypeMessage);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.PortNumber, frame_sensor_detect.PortNumber);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.LengthData, frame_sensor_detect.LengthData);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[0], frame_sensor_detect.Data[0]); // TypeSensor
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[1], frame_sensor_detect.Data[1]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[2], frame_sensor_detect.Data[2]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[3], frame_sensor_detect.Data[3]);
    TEST_ASSERT_EQUAL_HEX8(frame_sensor_test.Data[4], frame_sensor_detect.Data[4]);
    TEST_ASSERT_EQUAL_HEX16(frame_sensor_test.CheckFrame, frame_sensor_detect.CheckFrame);

    Print_Data_Detect(frame_sensor_detect);

    /*-------------------------------------------------(END_DETECT_FRAME_ANSWERDATA)-----------------------------------------*/

    printf("\nEnd test_SEC_Message_Create_Frame_Answerdata_Sensor_6\n");
}


/* Hàm in data bản tin */
void Print_Data_Message(uint8_t *arr, uint8_t length)
{
    printf("- Create -\n");

    for (uint8_t i = 0; i < length; i++)
    {
        if (arr[i] <= 0x0F)
        {
            printf("0%x ", arr[i]);
        }
        else
        {
            printf("%x ", arr[i]);
        }
    }
}

/**/
void Print_Data_Detect(sec_FrameMsg_t frame)
{
    printf("\n- Detect -");
    printf("\n Header       : %x ", frame.StartFrame);
    printf("\n TypeMessage  : %x ", frame.TypeMessage);
    printf("\n PortNumber   : %x ", frame.PortNumber);
    printf("\n LengthData   : %x ", frame.LengthData);
    
    switch (frame.TypeMessage)
    {
        case TYPE_MESSAGE_ANSWER_TYPE:
            printf("\n TypeSensor   : %x ", frame.Data[0]);
        break;

        case TYPE_MESSAGE_ANSWER_DATA:
            printf("\n TypeSensor   : %x ", frame.Data[0]);

            switch (frame.Data[0])
            {
                case SENSOR_1: case SENSOR_2: case SENSOR_3: case SENSOR_4: case SENSOR_5: case SENSOR_6:
                    float data_float = Sec_Convert_From_Bytes_To_Float(frame.Data[1], frame.Data[2], frame.Data[3], frame.Data[4]);
                    printf("\n DataSensor : %f ", data_float);
                break;
            }
        break;
    }

    printf("\n CheckFrame   : %x ", frame.CheckFrame);
}

/*--------------------------------------------------------*/

void Print_Data_Message_Fsm(uint8_t *arr, uint8_t length)
{
    printf("\nGiá trị phần tử trong mảng được truyền qua USART: ");
    for (uint8_t i = 0; i < length; i++)
    {
        if (arr[i] <= 0x0F)
        {
            printf("0%x ", arr[i]);
        }
        else
        {
            printf("%x ", arr[i]);
        }
    }
}

/*--------------------------------------------------------*/

// #endif // TEST
