#ifndef _SEC_MENU_H_
#define _SEC_MENU_H_

#include "sec_sys.h"


#define DISPLAY1_SELECT_MANUAL 1 /*1: lua chon test thu cong*/
#define DISPLAY1_SELECT_AUTO   2 /*2: lua chon test tu dong*/
#define DISPLAY1_SELECT_NULL   0 /*cho nhap vao test auto or manual*/


extern uint8_t display0_select;  /*bien de chon auto or manual*/

extern uint8_t display1_flag;	 /*co de chay ham Run*/
extern uint8_t display1_select;  /*bien de chon cac Port*/
extern uint8_t display1_flag_auto;  /*co de kiem tra lan luot cac Port*/
extern uint8_t display1_count_port; /*bien diem chuyen chan Port tu dong*/

void Display0_Menu(void);		 /*ham hien thi menu 0*/
void Display0_Null_Run(void);	 /*ham xu li chon menu manual or auto*/

void Display1_Manual_Init(void); /*ham hien thi menu manual 1*/
void Display1_Manual_Run(void);  /*ham xu li cac lua chon Port*/

void Display1_Auto_Init(void);   /*ham hien thi menu auto 1*/
void Display1_Auto_Run(void);    /*ham xu li cac lua chon Port*/

uint8_t Check_Number_Receive(void); /*ham tra ve cac gia tri gui vao*/


#endif
