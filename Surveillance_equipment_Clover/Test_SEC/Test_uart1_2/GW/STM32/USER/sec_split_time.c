#include "sec_split_time.h"


sec_FrameMsg_t frame_detect;
sec_status_time_e status_time = START;

Array_port_t arr_port[] = 
{
	{GPIOB, GET_GPIO_PORTB_1},
	{GPIOB, GET_GPIO_PORTB_2},
	{GPIOB, GET_GPIO_PORTB_3},
	{GPIOB, GET_GPIO_PORTB_4},
	{GPIOB, GET_GPIO_PORTB_5},
	{GPIOB, GET_GPIO_PORTB_6}
};


static uint8_t  Count_port = 1;
static uint16_t Count_time = 0;

static uint8_t Arr_status_old[10] = {0};
static uint8_t Arr_status_new[10] = {0};

static uint8_t Arr_flag_join_port[10] = {0};
static uint8_t Arr_flag_data_port[10] = {0};

static uint8_t Sensor_Port_2_Old = 0, Sensor_Port_2_New = 0;
static uint8_t Sensor_Port_3_Old = 0, Sensor_Port_3_New = 0;
static uint8_t Sensor_Port_6_Old = 0, Sensor_Port_6_New = 0;



/*-------------------------------------------------------------------------------------------------------------*/
void SEC_Split_frame_time(void)
{
	switch (status_time)
	{
		case START:
			Count_time++;

			if(Count_time == 100)
			{
				printf("START\n");
				Count_time = 0;
				status_time = JOIN;
			}
		break;
			
		case JOIN:
			Count_time++;
			
			/*------------------- (PORT 3) -------------------*/
//			if(Arr_flag_join_port[3] == 0)
//			{  
//				SEC_GPIO_Set_ON(3);
//				Create_Frame_AskSensor_Port_3();	// Tao va gui ban tin hoi TypeSensor
//				
//				if(sec_vrAll_fsm_t.Flag_fsm_True == 1)
//				{
//					printf("Nhan3\n");
//					/* Giai ma ban tin nhan duoc */
//					SEC_Message_Detect_Frame(sec_vrAll_fsm_t.array_out, &frame_detect);	
//					
//					Sensor_Port_3_New = frame_detect.Data[0]; // Ghi nho TypeSensor 
//					Sensor_Port_3_Old = Sensor_Port_3_New;
//					/* Hien thi ban tin duoc giai ma */
//					Print_Data_Detect(frame_detect);
//					
//					SEC_GPIO_Set_OFF(3);
//					Arr_flag_join_port[3] = 1;
//					Sec_Clear_Frame_Message(&frame_detect);	// Reset cac gia tri trong Struct 
//					
//					sec_vrAll_fsm_t.Flag_fsm_True = 0;
//				}
//			}
			
			if(Count_time == 100)
			{
				printf("JOIN\n");
				Count_time = 0;
				status_time = DATA;
			}
		break;
			
		case DATA:
			Count_time++;
			
			/*------------------- (PORT 3) -------------------*/
			if(Arr_flag_data_port[6] == 0)
			{  
				SEC_GPIO_Set_ON(6);
				Create_Frame_AskData_Port_6();	// Tao va gui ban tin hoi TypeSensor
				
				if(sec_vrAll_fsm_t.Flag_fsm_True == 1)
				{
					printf("Data3\n");
					/* Giai ma ban tin nhan duoc */
					SEC_Message_Detect_Frame(sec_vrAll_fsm_t.array_out, &frame_detect);	
					
					Print_Data_Detect(frame_detect);
					
					SEC_GPIO_Set_OFF(6);
					Arr_flag_data_port[6] = 1;
					Sec_Clear_Frame_Message(&frame_detect);	// Reset cac gia tri trong Struct 
					
					sec_vrAll_fsm_t.Flag_fsm_True = 0;
				}
			}
			
			if(Count_time == 100)
			{
				printf("DATA\n");
				Count_time = 0;
				status_time = BACK;
			}
		break;
			
		case BACK:
			Count_time++;
			Arr_flag_join_port[3] = 0;
			Arr_flag_data_port[6] = 0;
		
			if(Count_time == 100)
			{
				printf("BACK\n\n");
				Count_time = 0;
				status_time = START;
			}
		break;
	}
}

/*-------------------------------------------------------------------------------------------------------------*/
void SEC_Read_status_port(uint8_t vtri)
{	
	if(GPIO_ReadInputDataBit(GPIOB, arr_port[vtri - 1].Port_number) == 0)
	{
		Arr_status_new[vtri] = 1;
	}
	else
	{
		Arr_status_new[vtri] = 0;
	}
}

