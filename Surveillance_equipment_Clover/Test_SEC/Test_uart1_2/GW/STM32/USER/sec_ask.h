#ifndef _SEC_ASK_H_
#define _SEC_ASK_H_

#include "sec_sys.h"
#include "sec_convert.h"
#include "sec_message.h"

extern uint8_t Flag_Ask_TypeSensor;

uint8_t Create_Frame_AskSensor_Port_1(void);
uint8_t Create_Frame_AskSensor_Port_2(void);
uint8_t Create_Frame_AskSensor_Port_3(void);
uint8_t Create_Frame_AskSensor_Port_4(void);
uint8_t Create_Frame_AskSensor_Port_5(void);
uint8_t Create_Frame_AskSensor_Port_6(void);

uint8_t Create_Frame_AskData_Port_1(void);
uint8_t Create_Frame_AskData_Port_2(void);
uint8_t Create_Frame_AskData_Port_3(void);
uint8_t Create_Frame_AskData_Port_4(void);
uint8_t Create_Frame_AskData_Port_5(void);
uint8_t Create_Frame_AskData_Port_6(void);

void Print_Data_Message(uint8_t *arr, uint8_t length);
void Print_Data_Detect(sec_FrameMsg_t frame);

#endif
