#ifndef _SEC_SPLIT_TIME_H_
#define _SEC_SPLIT_TIME_H_

#include "sec_sys.h"

typedef enum
{
	START = 1,
	JOIN,
	DATA,
	BACK,
}sec_status_time_e;

typedef struct
{
	GPIO_TypeDef 	*GPIOx;
	uint16_t 		Port_number;	
}Array_port_t;

void SEC_Read_status_port(uint8_t vtri);
void SEC_Split_frame_time(void);


#endif
