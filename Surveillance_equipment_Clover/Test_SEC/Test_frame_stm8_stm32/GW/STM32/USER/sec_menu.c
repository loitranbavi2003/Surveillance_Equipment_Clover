#include "sec_menu.h"



uint8_t display0_select = 0;

uint8_t display1_flag = 0;
uint8_t display1_select = 0;
uint8_t display1_count_port = 1;  /*ham auto*/
uint8_t display1_flag_auto = 0;   /*ham auto*/

uint32_t display1_count_time = 0; /*ham auto*/


void Display0_Null_Run(void)
{
	if(display0_select == DISPLAY1_SELECT_MANUAL)
	{
		Display1_Manual_Init();
	}
	else if(display0_select == DISPLAY1_SELECT_AUTO)
	{
		Display1_Auto_Init();
	}
}

/*------------------------------------------ ham hien thi menu chinh ---------------------------------------*/
void Display0_Menu(void)
{
	printf("\n++++++++++++++++++++++++++++++++++\n");
	printf("+------ Select how to test ------+\n");
	printf("+                                +\n");
	printf("+      - Manual : 1              +\n");
	printf("+      - Auto   : 2              +\n");
	printf("+                                +\n");
	printf("+--------------------------------+\n");
	
	display0_select = DISPLAY1_SELECT_NULL;
}

/*------------------------------------------- ham test thu cong ----------------------------------------------*/
void Display1_Manual_Init(void)
{
	printf("\n+++++++++++ - MANUAL - +++++++++++\n");
	printf("+------- Select Port test -------+\n");
	printf("+                                +\n");
	printf("+      - Port1 : 1               +\n");
	printf("+      - Port2 : 2               +\n");
	printf("+      - Port3 : 3               +\n");
	printf("+      - Port4 : 4               +\n");
	printf("+      - Port5 : 5               +\n");
	printf("+      - Port6 : 6               +\n");
	printf("+      - Exit  : 7               +\n");
	printf("+--------------------------------+\n");
	
	display1_select = 0;
	display1_flag   = 0;
}

void Display1_Manual_Run(void)
{
	uint8_t display1_mode;
	
	if(display1_flag == 1)
	{
		display1_flag = 0;
		
		SEC_Test_Gpio_Control(GPIO_CONTROL_ENA_ALL); /*cap nguon cho 6 Port*/
		
		if(display1_select == 1)
		{
//			Gw_GPIO_Set_ON(1);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_1) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_1 Done!\n");
					Flag2_Receive = 0;
				}
				else
				{
					printf(" Port_1 error in!\n");
				}
			}
			else
			{
				printf(" Port_1 error out!\n");
			}

			display1_mode = 10;
		}
		else if(display1_select == 2)
		{
//			Gw_GPIO_Set_ON(2);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_2) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_2 Done!\n");
					Flag2_Receive = 0;
				}
				else
				{
					printf(" Port_2 error in!\n");
				}
			}
			else
			{
				printf(" Port_2 error out!\n");
			}

			display1_mode = 10;
		}
		else if(display1_select == 3)
		{
//			Gw_GPIO_Set_ON(3);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_3) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_3 Done!\n");
					Flag2_Receive = 0;
				}
				else
				{
					printf(" Port_3 error in!\n");
				}
			}
			else
			{
				printf(" Port_3 error out!\n");
			}

			display1_mode = 10;
		}
		else if(display1_select == 4)
		{
//			Gw_GPIO_Set_ON(4);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_4) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_4 Done!\n");
					Flag2_Receive = 0;
				}
				else
				{
					printf(" Port_4 error in!\n");
				}
			}
			else
			{
				printf(" Port_4 error out!\n");
			}

			display1_mode = 10;
		}
		else if(display1_select == 5)
		{
//			Gw_GPIO_Set_ON(5);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_5) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_5 Done!\n");
					Flag2_Receive = 0;
				}
				else
				{
					printf(" Port_5 error in!\n");
				}
			}
			else
			{
				printf(" Port_5 error out!\n");
			}

			display1_mode = 10;
		}
		else if(display1_select == 6)
		{
//			Gw_GPIO_Set_ON(6);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_6) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_6 Done!\n");
					Flag2_Receive = 0;
				}
				else
				{
					printf(" Port_6 error in!\n");
				}
			}
			else
			{
				printf(" Port_6 error out!\n");
			}

			display1_mode = 10;
		}
		else if(display1_select == 7)
		{
			display1_mode = 1;
			printf(" Exit \n");
		}
		
		if(display1_mode == 10 || display1_select > 7) /*chi xet th 'display1_mode = 10' vi ham 'Check_Number_Receive()' ko tra ve gia tri > 7*/
		{
			Display1_Manual_Init();
		}
		else if(display1_mode == 1)
		{
			SEC_Test_Gpio_Control(GPIO_CONTROL_DIS_ALL);
			Display0_Menu();
		}
	}
}


/*------------------------------------------- ham test tu dong ----------------------------------------------*/

void Display1_Auto_Init(void)
{
	printf("\n++++++++++++ - AUTO - ++++++++++++\n");
	printf("+--------------------------------+\n");
	printf("+                                +\n");
	printf("+   - Plug in the Port%d          +\n", display1_count_port);
	printf("+   - Exit : 7                   +\n");
	printf("+                                +\n");
	printf("+--------------------------------+\n");
	
	display1_select = 0;
	display1_flag   = 0;
}

void Display1_Auto_Run(void)
{
	uint8_t display1_mode = 0;
	
	display1_count_time++;
	
	if(display1_flag == 1)
	{
		display1_flag = 0;
		
		if(display1_count_port == 1)
		{
			SEC_GPIO_Set_ON(1);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_1) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_1 Done!\n");
					Flag2_Receive = 0;
					display1_flag_auto = 1;
				}
				else
				{
					printf(" Port_1 error in!\n");
					display1_flag_auto = 1;
				}
			}
			
			SEC_GPIO_Set_OFF(1);
			display1_mode = 10;
		}
		else if(display1_count_port == 2)
		{
			SEC_GPIO_Set_ON(2);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_2) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_2 Done!\n");
					Flag2_Receive = 0;
					display1_flag_auto = 1;
				}
				else
				{
					printf(" Port_2 error in!\n");
					display1_flag_auto = 1;
				}
			}
			
			SEC_GPIO_Set_OFF(2);
			display1_mode = 10;
		}
		else if(display1_count_port == 3)
		{
			SEC_GPIO_Set_ON(3);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_3) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_3 Done!\n");
					Flag2_Receive = 0;
					display1_flag_auto = 1;
				}
				else
				{
					printf(" Port_3 error in!\n");
					display1_flag_auto = 1;
				}
			}
			
			SEC_GPIO_Set_OFF(3);
			display1_mode = 10;
		}
		else if(display1_count_port == 4)
		{
			SEC_GPIO_Set_ON(4);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_4) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_4 Done!\n");
					Flag2_Receive = 0;
					display1_flag_auto = 1;
				}
				else
				{
					printf(" Port_4 error in!\n");
					display1_flag_auto = 1;
				}
			}
			
			SEC_GPIO_Set_OFF(4);
			display1_mode = 10;
		}
		else if(display1_count_port == 5)
		{
			SEC_GPIO_Set_ON(5);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_5) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_5 Done!\n");
					Flag2_Receive = 0;
					display1_flag_auto = 1;
				}
				else
				{
					printf(" Port_5 error in!\n");
					display1_flag_auto = 1;
				}
			}
			
			SEC_GPIO_Set_OFF(5);
			display1_mode = 10;
		}
		else if(display1_count_port == 6)
		{
			SEC_GPIO_Set_ON(6);
			
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_6) == 0)
			{	
				Gw_Test_Rs485_Send("Check!");
				SEC_DELAY_ms(300);
					
				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
				{
					printf(" Port_6 Done!\n");
					Flag2_Receive = 0;
					display1_flag_auto = 1;
				}
				else
				{
					printf(" Port_6 error in!\n");
					display1_flag_auto = 1;
				}
			}
			
			SEC_GPIO_Set_OFF(6);
			display1_mode = 10;
		}
		
		if(display1_flag_auto == 1 || display1_count_time > 2500000)
		{
			if(display1_count_time > 2500000)
			{
				printf(" Port_%d error out!\n", display1_count_port);
			}
			if(display1_count_port < 6)
			{
				display1_count_port++;
			}
			else
			{
				display1_count_port = 1;
				display1_mode = 0;
				Display0_Menu();
			}
			
			display1_count_time = 0;
			display1_flag_auto = 0;
		}
		else if(display1_select == 7)
		{
			display1_count_port = 1;
			printf(" Exit \n");
			Display0_Menu();
		}
		
		if(display1_mode == 10 && display1_count_time < 1) 
		{
			Display1_Auto_Init();
		}
	}
}


/*--------- ham kiem tra ki tu duoc gui vao -----------*/
uint8_t Check_Number_Receive(void)
{
	uint8_t number = 0;
	if(Flag1_Receive == 1)
	{
		if(strstr(Array1_Receive,"1")!= NULL)
		{
			number = 1;
		}
		else if(strstr(Array1_Receive,"2")!= NULL)
		{
			number = 2;
		}
		else if(strstr(Array1_Receive,"3")!= NULL)
		{
			number = 3;
		}
		else if(strstr(Array1_Receive,"4")!= NULL)
		{
			number = 4;
		}
		else if(strstr(Array1_Receive,"5")!= NULL)
		{
			number = 5;
		}
		else if(strstr(Array1_Receive,"6")!= NULL)
		{
			number = 6;
		}
		else if(strstr(Array1_Receive,"7")!= NULL)
		{
			number = 7;
		}
		
		Flag1_Receive = 0;
	}
	return number;
}





/*--------------------------------------- end ---------------------------------------------*/



//void fn_gw_menu_handmade(void)
//{
//	if(Flag1_Receive == 1)
//	{	
//		printf("<->");
//		
//		if(strstr(Array1_Receive,"1")!= NULL)
//		{
//			Gw_GPIO_Set_ON(1);
//			
//			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_1) == 0)
//			{	
//				Gw_Test_Rs485_Send("Check!");
//				Gw_DELAY_ms(300);
//					
//				if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
//				{
//					printf("Port_1 Done!");
//					Flag2_Receive = 0;
//				}
//				else
//				{
//					printf("Port_1 error in!");
//				}
//			}
//			else
//			{
//				printf("Port_1 error out!");
//			}
//			
//			vrui_flag_menu_handmade = 1;
//		}
//	}
//	
//	Gw_GPIO_Set_ON(6);
//	Gw_Test_Rs485_Send("Check!");
//	Gw_DELAY_ms(300);
////	Gw_Test_Rs485_Get();
//	if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
//	{
//		printf("Oke\n");
//		Flag2_Receive = 0;
//		Gw_DELAY_ms(100);
//	}	
//}

