#ifndef _SEC_LOG_H_
#define _SEC_LOG_H_

#include "sec_sys.h"
#include "sec_message.h"

#define LOG_CREATE_FRAME 0

void SEC_LogError(char *Log, uint8_t port_number);
void SEC_LogDone(char *Log, uint8_t port_number);
void SEC_ClearFrameMessage(sec_FrameMsg_t *Frame);
void SEC_PrintfArrReceive(uint8_t *arr, uint8_t len);

#endif