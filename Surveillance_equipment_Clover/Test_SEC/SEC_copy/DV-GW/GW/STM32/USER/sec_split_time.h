#ifndef _SEC_SPLIT_TIME_H_
#define _SEC_SPLIT_TIME_H_

#include "sec_sys.h"

#define SENSOR_DISCONNECT 	0
#define SENSOR_JOIN 		1
#define SENSOR_CONNECT 		2

#define FRAME_SEND 1
#define FRAME_RECEIVE 1
#define NUMBER_PORT 7
#define TIME_SEND 3

typedef struct
{
	GPIO_TypeDef 	*GPIOx;
	uint16_t 		Port_number;	
}Array_port_t;

typedef enum
{
	STATUS_START = 1,
	STATUS_JOIN,
	STATUS_DISCONNECT,
	STATUS_DATA,
	STATUS_BACK,
}sec_status_time_e;


void SEC_SplitFrameTime(void);
void SEC_JoinPortSend(uint8_t port_number);
void SEC_DataPortSend(uint8_t port_number);
uint8_t SEC_JoinPortReceive(uint8_t port_number);
uint8_t SEC_DataPortReceive(uint8_t port_number);
void SEC_ReadStatusPort(uint8_t port_number);

#endif
