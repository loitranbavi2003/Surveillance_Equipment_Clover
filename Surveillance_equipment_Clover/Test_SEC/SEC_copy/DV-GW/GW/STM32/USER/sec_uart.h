#ifndef __SEC_UART_H__
#define __SEC_UART_H__


#ifdef __cplusplus
 extern "C" {
#endif

#include "stdio.h"
#include "string.h"
#include "sec_delay.h"
#include "stm32f10x.h"    
#include "sec_fsm.h"


#define  UART1_BAUDRATE (9600)
#define  UART2_BAUDRATE (9600)
#define  UART3_BAUDRATE (9600)
	 
#define STRING_SIZE 50
#define GPIO_CONTROL_RS485 GPIO_Pin_4
#define PORT_CONTROL_RS485 GPIOA


extern char flag1_receive;
extern char array1_receive[];
extern char count1_data;

extern char flag2_receive;
extern char array2_receive[];
extern char count2_data;

extern char flag3_receive;
extern char array3_receive[];
extern char count3_data;
	
	 
void SEC_UartInit(void);
void SEC_SetGpioPortRs485Init(void);

void SEC_UartInit1(unsigned int BaudRates);
void SEC_Uart1SendChar(char data);
void SEC_Uart1SendString(char *data);
void SEC_Uart1SendByte(uint8_t *data, uint8_t sizes); 

void SEC_UartInit2(unsigned int BaudRates);
void SEC_Uart2SendChar(char data);
void SEC_Uart2SendString(char *data);
void SEC_Uart2SendByte(uint8_t *data, uint8_t sizes); 
uint8_t SEC_Uart2Compare(char *string);

void SEC_UartInit3(unsigned int BaudRates);


#ifdef __cplusplus
}
#endif

#endif

