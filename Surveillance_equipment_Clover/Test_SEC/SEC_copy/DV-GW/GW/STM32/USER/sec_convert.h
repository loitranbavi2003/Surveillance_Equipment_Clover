#ifndef SEC_CONVERT_H
#define SEC_CONVERT_H

#ifdef __cplusplus
    extern "C" {
#endif

#include<stdio.h>
#include<stdint.h>
#include "math.h"
#include "sec_define.h"            
#include "stm32f10x_gpio.h"

typedef union 
{
	float data_float;
	uint8_t bytes[4];	
} data_format_float_bytes;

typedef union 
{
    uint32_t data_int;
    uint8_t bytes[4];
} data_format_int_bytes;

typedef union 
{
    uint16_t data_uint16;
    uint8_t bytes[2];
} data_format_uint8_16_t;

/******************************************************************************/
char* SEC_ConverIntToStr(const int32_t data, uint8_t d);
char* SEC_ConverFloatToStr(const float data, uint8_t afterpoint);

int32_t SEC_ConverStrToInt(const char *data);
float 	SEC_ConverStrToFloat(const char *data);
/******************************************************************************/

uint8_t* SEC_ConvertFromFloatToBytes(float data);

float SEC_ConvertFromBytesToFloat(uint8_t data1, uint8_t data2, uint8_t data3, uint8_t data4);

uint8_t* SEC_ConvertFromIntToBytes(int data);

uint32_t SEC_ConvertFromBytesToInt(uint8_t data1, uint8_t data2, uint8_t data3, uint8_t data4);

uint8_t* SEC_ConvertFromUint16ToBytes(int data);

uint16_t SEC_ConvertFromBytesToUint16(uint8_t data1, uint8_t data2);

uint8_t SEC_ConvertFromBytesToUint8(uint8_t data);

#ifdef __cplusplus
}
#endif

#endif // SEC_CONVERT_H
