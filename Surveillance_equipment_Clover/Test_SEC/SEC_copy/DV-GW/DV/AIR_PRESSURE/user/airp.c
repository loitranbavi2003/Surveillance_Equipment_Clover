#include "airp.h"



// Function to read pressure value from the sensor
float ReadPressure() {
  float pressure;
  
  // Start ADC conversion on the configured channel (e.g., channel 0)
  ADC1_CSR = 0;
  ADC1_CSR |= (0x01 << 0);  // Set ADC channel to 0 (connected to Out pin of the sensor)
  ADC1_CR1 |= ADC1_CR1_ADON; // Start ADC conversion
  
  // Wait for ADC conversion to complete
  while (!(ADC1_CSR & ADC1_CSR_EOC))
    ;
  
  // Read the digital value from the ADC data register
  pressure = ADC1_DRH << 8;
  pressure |= ADC1_DRL;
  
  return pressure;
}

