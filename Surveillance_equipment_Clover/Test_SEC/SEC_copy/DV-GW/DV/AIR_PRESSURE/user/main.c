#include "sec_sys.h"

//Sensor Soil Moisture
//D6-rx
//D5-tx

int main( void )
{
    SEC_Sys_Init();    
    SEC_InterruptPriority(18, 0);
    SEC_InterruptPriority(23, 1);
    
    while (1)
    {     
        //SEC_Sys_Run();
        
    #if PRINTF_DATA_SOIL_MOISTURE
        UART_Send_Number_Float(adc_ain3_data);
        UART_Send_String("\n\n");
        SEC_Delay_Ms(500);
    #endif
    }
}
