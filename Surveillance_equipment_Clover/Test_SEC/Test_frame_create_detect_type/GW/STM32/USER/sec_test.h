
#ifndef __SEC_TEST__H__
#define __SEC_TEST__H__

#ifdef __cplusplus
 extern "C" {
#endif


#include "sec_sys.h"




void Gw_Test_Run(void);
void Gw_Test_Uart(void);
void Gw_Test_Gpio_ReadStt(uint8_t _mode_);


void Gw_Test_Rs485_Get(void);
void Gw_Test_Rs485_Send(char *data);


#ifdef __cplusplus
}
#endif

#endif

/********************************* END OF FILE ********************************/
/******************************************************************************/
