#include "sec_split_time.h"


sec_FrameMsg_t frame_detect;
sec_status_time_e status_time = START;

Array_port_t arr_port[] = 
{
	{GPIOB, GET_GPIO_PORTB_1},
	{GPIOB, GET_GPIO_PORTB_2},
	{GPIOB, GET_GPIO_PORTB_3},
	{GPIOB, GET_GPIO_PORTB_4},
	{GPIOB, GET_GPIO_PORTB_5},
	{GPIOB, GET_GPIO_PORTB_6}
};


static uint8_t  Count_port = 1;
static uint16_t Count_time = 0;

static uint8_t Arr_status_old[10] = {0};
static uint8_t Arr_status_new[10] = {0};

static uint8_t Arr_flag_join_port[10] = {0};
static uint8_t Arr_flag_data_port[10] = {0};

static uint8_t Sensor_Port_2_Old = 0, Sensor_Port_2_New = 0;
static uint8_t Sensor_Port_3_Old = 0, Sensor_Port_3_New = 0;
static uint8_t Sensor_Port_6_Old = 0, Sensor_Port_6_New = 0;



/*-------------------------------------------------------------------------------------------------------------*/
void SEC_Split_frame_time(void)
{
//	SEC_GPIO_Set_ON(6);
//	/* Tao va gui ban tin TypeSensor */
//	Create_Frame_AskSensor_Port_3();
//	//Create_Frame_AskData_Port_3();
//	printf("Gui3\n");

	if(sec_vrAll_fsm_t.Flag_fsm_True == 1)
	{
		printf("Nhan3\n");
		/* Giai ma ban tin nhan duoc */
		SEC_Message_Detect_Frame(sec_vrAll_fsm_t.array_out, &frame_detect);	
		/* Hien thi ban tin duoc giai ma */
		Print_Data_Detect(frame_detect);

		/* Reset cac gia tri trong Struct */
		Sec_Clear_Frame_Message(&frame_detect);	
		
		sec_vrAll_fsm_t.Flag_fsm_True = 0;
	}
}

/*-------------------------------------------------------------------------------------------------------------*/
void SEC_Read_status_port(void)
{	
	/* Tao va gui ban tin TypeSensor */
	Create_Frame_AskSensor_Port_3();
	//Create_Frame_AskData_Port_3();
	printf("Gui3\n");
}

