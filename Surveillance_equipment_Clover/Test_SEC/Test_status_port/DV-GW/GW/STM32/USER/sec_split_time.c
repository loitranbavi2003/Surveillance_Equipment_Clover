#include "sec_split_time.h"


sec_FrameMsg_t frame_detect;
sec_status_time_e status_time = START;
sec_status_port_e status_port = PORT_1;

Array_port_t arr_port[] = 
{
	{GPIOB, GET_GPIO_PORTB_1},
	{GPIOB, GET_GPIO_PORTB_2},
	{GPIOB, GET_GPIO_PORTB_3},
	{GPIOB, GET_GPIO_PORTB_4},
	{GPIOB, GET_GPIO_PORTB_5},
	{GPIOB, GET_GPIO_PORTB_6}
};

static uint8_t  Count_port = 1;
static uint16_t Count_time = 0;

static uint8_t Arr_status_old[10] = {0};
static uint8_t Arr_status_new[10] = {0};

void SEC_Split_frame_time(void)
{
	switch (status_time)
	{
		case START:
			/* Doc trang thai cua cac Port */
			SEC_Read_status_port(Count_port);
			Count_port++;
			SEC_GPIO_Set_ON(3);
			if(Count_port == 7)
			{
				printf("START\n");
				Count_port = 1;
				status_time = JOIN;
			}
		break;
		
		case JOIN:
			Count_time++;
			if(Count_time == 200)
			{
				printf("JOIN\n");
				Count_time = 0;
				status_time = DATA;
			}
		break;
		
		case DATA:
			Count_time++;
			if(Count_time == 200)
			{
				printf("DATA\n");
				for(int i = 1; i <= 6; i++)
				{
					printf("%d ", Arr_status_old[i]);
				}
				printf("\n");
				for(int i = 1; i <= 6; i++)
				{
					printf("%d ", Arr_status_new[i]);
				}
				printf("\n");
				
				Count_time = 0;
				status_time = BACK;
			}
		break;
		
		case BACK:
			/* Doc trang thai cua cac Port */
			SEC_Read_status_port(Count_port);
			Count_port++;
			
			if(Count_port == 7)
			{
				printf("BACK\n");
				/* Cap nhat lai trang thai cua cac Port*/
				for(int i = 1; i <= 6; i++)
				{
					Arr_status_old[i] = Arr_status_new[i];
				}

				Count_port = 1;
				status_time = START;
			}
		break;
	}
}

void SEC_Join_Port(void)
{
	switch (status_port)
	{
		case PORT_1:
			if(Arr_status_old[1] == 0 && Arr_status_new[1] == 1)
			{
				status_port = PORT_2;
			}
			else
			{
				status_port = PORT_2;
			}
		break;
		
		case PORT_2:
			
		break;
		
		case PORT_3:
			
		break;
		
		case PORT_4:
			
		break;
		
		case PORT_5:
			
		break;
		
		case PORT_6:
			
		break;
	}
}

void SEC_Read_status_port(uint8_t port_number)
{	
	if(GPIO_ReadInputDataBit(GPIOB, arr_port[port_number - 1].Port_number) == 0)
	{
		Arr_status_new[port_number] = 1;
	}
	else
	{
		Arr_status_new[port_number] = 0;
	}
}

