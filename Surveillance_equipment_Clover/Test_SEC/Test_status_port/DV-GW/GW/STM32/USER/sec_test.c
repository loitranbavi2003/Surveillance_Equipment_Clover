#include "sec_test.h"


uint16_t GPIO_PORTB_Arr[6]  = {GET_GPIO_PORTB_1, GET_GPIO_PORTB_2, GET_GPIO_PORTB_3, GET_GPIO_PORTB_4, GET_GPIO_PORTB_5, GET_GPIO_PORTB_6};

static void Gw_Test_All(void);

void Gw_Test_Uart(void)
{
	SEC_Uart1_SendString("hello\n");
}



void Gw_Test_Gpio_ReadStt(uint8_t _mode_)
{
	uint8_t i = 0, arrget[6] = {0, 0, 0, 0, 0, 0};
	char arrchar[20];
	if(_mode_)
	{
		for(i=0; i<6; i++)
		{
			arrget[i] = GPIO_ReadInputDataBit(GPIOB, GPIO_PORTB_Arr[i]);
		}
		for(i=0; i<6; i++)
		{
			sprintf(arrchar,"Arr[%d]: %d\n", i, arrget[i]);
			SEC_Uart1_SendString(arrchar);
		}
		SEC_Uart1_SendString("-----------------------------------------\n");
	}
}

void Gw_Test_Rs485_Get(void)
{
	if(Flag2_Receive == 1)
	{
		Flag2_Receive = 0;
		SEC_Uart1_SendString(Array2_Receive);
	}
}

void Gw_Test_Rs485_Send(char *data)
{
	SEC_Uart2_SendString(data);
}

static void Gw_Test_All(void)
{
	
}

void Gw_Test_Run(void)
{
	Gw_Test_Uart();
	SEC_Test_Gpio_Control(GPIO_CONTROL_ENA_ALL);
	//Gw_Test_Gpio_ReadStt(1);
	Gw_Test_Rs485_Send("Check!\n");
	SEC_DELAY_ms(300);
}




/*------------------------------------------------------------------------------------------------------------*/


//	Gw_GPIO_Set_ON(6);
//	Gw_Test_Rs485_Send("Check!");
//	Gw_DELAY_ms(300);
////	Gw_Test_Rs485_Get();
//	if(Flag2_Receive == 1 && UART2_Compare("Done") == 1)
//	{
//		printf("Oke\n");
//		Flag2_Receive = 0;
//		Gw_DELAY_ms(100);
//	}	
//}



/*------------------------------------------------------------------------------------------------------------*/