#ifndef _SEC_SPLIT_TIME_H_
#define _SEC_SPLIT_TIME_H_

#include "sec_sys.h"


typedef struct
{
	GPIO_TypeDef 	*GPIOx;
	uint16_t 		Port_number;	
}Array_port_t;

typedef enum
{
	START = 1,
	JOIN,
	DATA,
	BACK,
}sec_status_time_e;

typedef enum
{
	PORT_1 = 1,
	PORT_2,
	PORT_3,
	PORT_4,
	PORT_5,
	PORT_6,
} sec_status_port_e;

void SEC_Split_frame_time(void);
void SEC_Join_Port(void);
void SEC_Data_Port(void);
void SEC_Read_status_port(uint8_t port_number);

#endif
